export class Specifications {
  static fetch() {
    try {
      return fetch('https://virtual-showcase.firebaseio.com/specifications.json')
        .then(response => response.json())
        .then(response => {
          localStorage.setItem('specifications', JSON.stringify(response))
        }) || {};
    } catch (error) {
      console.log(error);
    }
  }

  static getSpecifications() {
    let specifications = JSON.parse(localStorage.getItem('specifications'))
    const sortedObj = {};
    const arrSortedKeys = Object.keys(specifications).sort((a, b) => specifications[a].id - specifications[b].id);
    for (let i of arrSortedKeys) {
      sortedObj[i] = specifications[i];
    }
    return sortedObj;
  }
}