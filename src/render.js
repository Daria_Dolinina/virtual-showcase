import * as THREE from 'three';
import { GLTFLoader } from '../node_modules/three/examples/jsm/loaders/GLTFLoader';
import { DRACOLoader } from '../node_modules/three/examples/jsm/loaders/DRACOLoader';
import { OrbitControls } from '../node_modules/three/examples/jsm/controls/OrbitControls';

let camera, scene, renderer, texture, loader, dracoLoader, controls, object, sizeBound;
const $loaderPage = document.getElementById('loader');

function initScene(model) {
    scene = new THREE.Scene();
    scene.background = new THREE.Color(0xffffff);

    camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.1, 10000);
    camera.position.set(10, 20, 50);

    lights()

    renderScene()

    createBackground();

    sizeBound = new THREE.Vector3(30, 30, 30);

    loaderModel(model);
    glassCube();

    controls = new OrbitControls(camera, renderer.domElement);
    controls.minDistance = 30;
    controls.maxDistance = 100;
}

function lights() {
    scene.add(new THREE.HemisphereLight(0xffffff, 0x000000, 0.2));

    let light = new THREE.SpotLight(0xffffff, 0.2);
    light.position.set(-500, 500, 500);
    light.castShadow = true;
    scene.add(light);
}

function renderScene() {
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.outputEncoding = THREE.sRGBEncoding;
    renderer.shadowMap.enabled = true;
    renderer.setSize(window.innerWidth, window.innerHeight);

    document.body.appendChild(renderer.domElement);
}

function loaderModel(model) {
    $loaderPage.hidden = false;

    loader = new GLTFLoader();
    dracoLoader = new DRACOLoader();

    dracoLoader.setDecoderPath('/models');
    loader.setDRACOLoader(dracoLoader);

    if (object) scene.remove(object)

    loader.load(
        model.url,
        function (gltf) {
            object = gltf.scene;

            object.rotation.x = model.rotationX;

            object.traverse(n => {
                if (n.isMesh) {
                    objectColor(model.colors[0], model.corps)
                    n.material.envMap = texture;
                    n.material.needsUpdate = true;
                }
            });

            scaleToFit(object, sizeBound);
            scene.add(object);

        },
        function (xhr) {
            $loaderPage.innerHTML = `loading ${Math.floor(xhr.loaded / xhr.total * 100)}%`;
            if (xhr.loaded / xhr.total * 100 === 100) {
                setTimeout(() => {
                    $loaderPage.hidden = true;
                }, 500);
            }
        },
        function (error) {
            console.log('An error happened', error);
        }
    );
}

function objectColor(color, part) {
    object.traverse(n => {
        if (n.isMesh && part.includes(n.material.name)) {
            n.material.color.set(color);
        }
    });
}

function glassCube() {
    const glassCube = new THREE.Mesh(new THREE.BoxBufferGeometry(50, 50, 50), new THREE.MeshBasicMaterial({
        color: 0xddffff,
        depthTest: true,
        depthWrite: false,
        opacity: 0.25,
        transparent: true,
        envMap: texture,
        side: THREE.DoubleSide
    }));

    glassCube.position.y = -40;
    glassCube.receiveShadow = true;

    scene.add(glassCube);
}

function scaleToFit(obj, bound) {
    let box = new THREE.Box3().setFromObject(obj);
    let size = new THREE.Vector3();
    box.getSize(size);
    let vScale = new THREE.Vector3().copy(bound).divide(size);
    let scale = Math.min(vScale.x, Math.min(vScale.y, vScale.z));
    obj.scale.setScalar(scale);
    let boxObj = new THREE.Box3().setFromObject(object);
    object.position.sub(boxObj.getCenter(size));
}

function createBackground() {
    texture = new THREE.TextureLoader().load('./src_img_scene.jpg');
    const sphere = new THREE.Mesh(new THREE.SphereGeometry(-150, 32, 32), new THREE.MeshBasicMaterial({ map: texture }));
    sphere.rotation.y = 2.7;
    sphere.position.y = 40;
    texture.mapping = THREE.EquirectangularReflectionMapping;
    scene.add(sphere);
}

function animateScene() {
    requestAnimationFrame(animateScene);
    controls.update();
    renderer.render(scene, camera);
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );

}

window.addEventListener( 'resize', onWindowResize, false );

export { initScene, animateScene, loaderModel, objectColor }