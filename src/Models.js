export class Models {
  static fetch() {
    return fetch('https://virtual-showcase.firebaseio.com/models.json')
    .then(response => response.json())
    .then(response => {
      localStorage.setItem('models', JSON.stringify(response))
    }) || {}
  }
  
  static allModels() {
    return JSON.parse(localStorage.getItem('models'))
  }
  
  static firstModel() {
    const models = this.allModels();
    return models[Object.keys(models)[0]]
  }
  
  static currentModel(key) {
    const models = this.allModels()
    return models[key]
  }
}