'use strict';
import './style.css';
import './src_img_scene.jpg';

import { initScene, animateScene, loaderModel, objectColor } from './render';
import { Models } from './Models'
import { Specifications } from './Specifications'

let currentModel;

async function createSelect() {
    const $select = document.getElementById('selectModel');
    let $option = document.createElement('option');
    const models = Models.allModels();

    for (const key of Object.keys(models)) {
        const optionClone = $option.cloneNode();
        optionClone.setAttribute('value', key)
        optionClone.innerHTML = models[key].name
        $select.appendChild(optionClone)
    }
}

function createSpecificationsTable() {
    const $table = document.getElementById('tableSpecifications').children[0];
    const specifications = Specifications.getSpecifications()
    $table.innerHTML = '';
    for (let key of Object.keys(specifications)) {
        $table.innerHTML += `<tr>
                <td>${specifications[key].text}</td>
                <td>${currentModel.specifications[key] || 'Нет'}</td>
            </tr>`;
    }
}

function createColors() {
    let colors = currentModel.colors;
    let $row = document.getElementById('colorsWrapper');
    $row.innerHTML = '';
    for (let i = 0; i < colors.length; i++) {
        $row.innerHTML += `<div class="color color${i}">
                <input 
                    type="radio"
                    id="radio${i}"
                    value="${colors[i]}"
                    name="color"
                    ${i === 0 ? 'checked' : ''}>
                <label 
                    for="radio${i}" 
                    class="color-label" 
                    style="background-color: ${colors[i]}">
                </label>
            </div>`
    }
    let $inputs = $row.querySelectorAll('input')
    $inputs.forEach(el => {
        el.addEventListener('change', function() {
            objectColor(this.value, currentModel.corps);
        })
    })
}

document.addEventListener('DOMContentLoaded', async function () {
    if (!localStorage.getItem('models')) {
        await Models.fetch();
    }
    if (!localStorage.getItem('specifications')) {
        await Specifications.fetch();
    }

    currentModel = Models.firstModel()
    initScene(currentModel);
    animateScene();

    createSelect();
    createSpecificationsTable();
    createColors();

    const $select = document.getElementById('selectModel');
    $select.addEventListener('change', function () {
        currentModel = Models.currentModel(this.value);
        loaderModel(currentModel);
        createSpecificationsTable();
        createColors();
    });

});
