let path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HTMLWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpckPlugin = require('copy-webpack-plugin');

const isDev = process.env.NODE_ENV === 'development';
const isProd = !isDev;

let config = {
    context: path.resolve(__dirname, 'src'),
    entry: {
        index: ['babel-polyfill', './index.js']
    },
    output: {
        path: path.resolve(__dirname, './public'),
        filename: 'main.js',
        // publicPath: 'public/' // для отсутсвия создания папки
    },
    devServer: {
        port: 4000,
        overlay: true, // для отображения ошибок на экране
    },
    module: {
        rules: [{
            test: /\.js$/,
            loader: "babel-loader",
            options: {
                presets: ["@babel/preset-env"]
            },
            exclude: '/node_modules/' // чтобы не прогонять готовые библиотеки через babel
        },
        {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                use: "css-loader"
            })
        },
        {
            test: /\.(png|jpg|svg|gif)$/,
            loader: 'file-loader',
            options: {
                name: '[path][name].[ext]',
            },
        },
        ]
    },
    plugins: [
        new ExtractTextPlugin("style.css"),
        new HTMLWebpackPlugin({
            template: 'index.html'
        }),
        new CleanWebpackPlugin(),
        new CopyWebpckPlugin({
            patterns: [
                {
                    from: path.resolve(__dirname, 'src/models/'),
                    to: path.resolve(__dirname, 'public/models/')
                },

            ]
        })
    ]
};

module.exports = (env, options) => {
    let production = options.mode === "production";

    config.devtool = production ? false : 'eval-sourcemap';

    return config;
}